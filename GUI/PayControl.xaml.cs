﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataCore.Persistance;

namespace GUI
{
    /// <summary>
    /// Interaction logic for PayControl.xaml
    /// </summary>
    public partial class PayControl : UserControl
    {
        private TsContext TsContext { get; set; }

        public PayControl(TsContext tsContext)
        {
            TsContext = tsContext;
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            TsContext.Flats.Load();
            TsContext.Payments.Load();

             if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
             {
             	//Load your data here and assign the result to the CollectionViewSource.
             	System.Windows.Data.CollectionViewSource myCollectionViewSource = (System.Windows.Data.CollectionViewSource)this.Resources["flatViewSource"];
                 myCollectionViewSource.Source = TsContext.Flats.ToList();
             }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace GUI.Pages
{
    class NavigationPage
    {
        private readonly Uri flatPageUri = new Uri("pack://application:,,,/GUI;component/Pages/flatpage.xaml");
        private Uri addFlatPageUri = new Uri("pack://application:,,,/GUI;component/Pages/addflatpage.xaml");
        private Uri servicePageUri = new Uri("pack://application:,,,/GUI;component/Pages/servicepage.xaml");
        private Uri addServicePageUri = new Uri("pack://application:,,,/GUI;component/Pages/addservicepage.xaml");
        private readonly Frame _frame;

        public NavigationPage(Frame frame)
        {
            _frame = frame;
        }

        public NavigationPage(Frame frame, Uri uri)
        {
            frame.Source = uri;
        }

        public void SelectFlatPage()
        {
            _frame.Source = flatPageUri;
        }
        public void SelectAddFlatPage()
        {
            _frame.Source = addFlatPageUri;
        }
        public void SelectServicePage()
        {
            _frame.Source = servicePageUri;
        }
        public void SelectAddServicePage()
        {
            _frame.Source = addServicePageUri;
        }
    }
}

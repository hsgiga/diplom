﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataCore.Model;
using DataCore.Persistance;

namespace GUI
{
    /// <summary>
    /// Interaction logic for AddFlatPage.xaml
    /// </summary>
    public partial class AddFlatPage : Page
    {
        private TsContext context = new TsContext();
        private Flat flat = new Flat();
        public AddFlatPage()
        {
            InitializeComponent();
            this.DataContext = flat;
        }

        private void BtnAddFlat_OnClick(object sender, RoutedEventArgs e)
        {
            flat.Number = Convert.ToInt16(tblNumber.Text);
            flat.Area = Convert.ToSingle(tblArea.Text);
            context.Flats.Add(flat);
            context.SaveChanges();
        }

        private void AddFlatPage_OnLoaded(object sender, RoutedEventArgs e)
        {
            CollectionViewSource flatViewSource = ((CollectionViewSource)(this.FindResource("FlatViewSource")));
            // Load data by setting the CollectionViewSource.Source property:
            context.Flats.Load();
            flatViewSource.Source = context.Flats.Local;
        }
    }
}

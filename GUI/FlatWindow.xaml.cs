﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DataCore.Model;
using DataCore.Persistance;

namespace GUI
{
    /// <summary>
    /// Interaction logic for FlatWindow.xaml
    /// </summary>
    public partial class FlatWindow : Window
    {
        private TsContext TsContext { get; set; }

        public FlatWindow(TsContext context)
        {
            TsContext = context;
            InitializeComponent();
            btn_SaveAddFlat.Click += Btn_AddFlat_OnClick;
            this.Title = "Добавить квартиру";
            btn_SaveAddFlat.Content = "Добавить";
        }

        public FlatWindow(TsContext context, Flat flat)
        {
            TsContext = context;
            TsContext.Flats.Load();
            InitializeComponent();
            if (flat != null) FlatGrid.DataContext = TsContext.Flats.Find(flat.Id);
            btn_SaveAddFlat.Click += BtnSaveFlatOnClick;
            this.Title = "Изменить данные о квартире";
            btn_SaveAddFlat.Content = "Сохранить";
        }

        private void BtnSaveFlatOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            TsContext.SaveChanges();
        }

        private void CloseButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Btn_AddFlat_OnClick(object sender, RoutedEventArgs e)
        {
            var flat=new Flat();
            flat.Number = int.Parse(tb_Number.Text);
            flat.Area = double.Parse(tb_Area.Text,CultureInfo.InvariantCulture);
            flat.CountRoom = int.Parse(tb_CountRoom.Text);
            flat.Floor = int.Parse(tb_Floor.Text);
            var owner = new Owner
                {
                    FirstName = tb_FirstName.Text,
                    LastName = tb_LastName.Text,
                    MiddleName = tb_MiddleName.Text
                };
            flat.Owner = owner;
            flat.PeopleLiving = int.Parse(tb_Living.Text);
            flat.PeopleRegister = int.Parse(tb_Registered.Text);
            TsContext.Flats.Add(flat);
            TsContext.SaveChanges();
            InitializeComponent();
        }
    }
}


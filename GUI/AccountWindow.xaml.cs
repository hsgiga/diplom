﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DataCore.Model;
using DataCore.Persistance;

namespace GUI
{
    /// <summary>
    /// Interaction logic for AccountWindow.xaml
    /// </summary>
    public partial class AccountWindow : Window
    {
        private TsContext TsContext { get; set; }

        public AccountWindow()
        {
            InitializeComponent();
        }

        public AccountWindow( TsContext tsContext)
        {
            tsContext.Flats.Load();
            tsContext.Services.Load();
            tsContext.Tariffs.Load();

            
            TsContext = tsContext;
            
            InitializeComponent();

            flatView.ItemsSource = tsContext.Flats.ToList();
            flatView.Items.SortDescriptions.Add(new SortDescription("Number",ListSortDirection.Ascending));
            serviceView.ItemsSource = tsContext.Services.ToList();
        }

        private void BtnClose_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnCalc_OnClick(object sender, RoutedEventArgs e)
        {
            foreach (Flat flat in flatView.SelectedItems)
            {
                var account = new Account(06,2013,DateTime.Now);
                foreach (Service service in serviceView.SelectedItems)
                {
                    account.Add(new ProvidedService(service));
                }
                account.Calculation(flat);
                flat.Add(account);
            }
            TsContext.SaveChanges();
            this.Close();
        }

    }
}

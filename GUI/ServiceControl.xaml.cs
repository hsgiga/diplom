﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataCore.Persistance;

namespace GUI
{
    /// <summary>
    /// Interaction logic for ServiceControl.xaml
    /// </summary>
    public partial class ServiceControl : UserControl
    {
        private TsContext TsContext { get; set; }
        public ServiceControl(TsContext tsContext)
        {
            InitializeComponent();
            TsContext = tsContext;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            TsContext.Services.Load();
            TsContext.Tariffs.Load();
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                System.Windows.Data.CollectionViewSource myCollectionViewSource =
                    (System.Windows.Data.CollectionViewSource)this.Resources["serviceViewSource"];
                myCollectionViewSource.Source = TsContext.Services.ToList();
            }
        }
    }
}

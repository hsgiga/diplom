﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataCore;
using DataCore.Persistance;
using DataCore.Model;
using GUI.Pages;


namespace GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RibbonWindow
    {
        private readonly TsContext _context = new TsContext();
        //private NavigationPage NavPage { get; set; }
        private  FlatControl FlatControl { get; set; }
        private PayControl PayControl { get; set; }
        private FlatControlNew FlatControlNew { get; set; }
        private  ServiceControl ServiceControl{ get; set; }
        private AccountsControl AccountsControl { get; set; }

        public MainWindow()
        {
            InitializeComponent();
//          var mdc = new MainDataClass();
//            MainDataClass.InitContext();

            
            PayControl = new PayControl(_context);
            FlatControlNew = new FlatControlNew(_context);
            ServiceControl = new ServiceControl(_context);
            AccountsControl = new AccountsControl(_context);

        }

        private void MenuExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //e.AddedItems();
            //var navPage = new NavigationPage(FrameTs);
            var tabIndex = Ribbon.SelectedIndex;
            switch (tabIndex)
            {
                case 0:
                    MainGrid.Children.Clear();
                    MainGrid.Children.Add(FlatControlNew);
                    //NavPage.SelectFlatPage();
                    break;
                case 1:
                    MainGrid.Children.Clear();
                    MainGrid.Children.Add(ServiceControl);
                    //NavPage.SelectServicePage();
                    break;
                case 2:
                    
                    MainGrid.Children.Clear();
                    MainGrid.Children.Add(AccountsControl);
                    break;
                case 3:

                    MainGrid.Children.Clear();
                    MainGrid.Children.Add(PayControl);
                    break;
                default:
                    
                    break;
            }
     
        }
        private void btnAddFlat_OnClick(object sender, RoutedEventArgs e)
        {
            FlatWindow flatWindow = new FlatWindow(_context);
            flatWindow.Show();
        }
        private void btnDelFla_OnClick(object sender, RoutedEventArgs e)
        {
            FlatControlNew.Delete();
            //FlatControl.InitializeComponent();
            //MainDataClass.Delete((Flat)FlatControl.flatListView.SelectedItem);
        }

        private void RibbonWindow_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void BtnEditFlat_OnClick(object sender, RoutedEventArgs e)
        {
            FlatWindow flatWindow = new FlatWindow(_context, (Flat)FlatControlNew.flatListView.SelectedItem);
            flatWindow.Show();
        }

        private void BtnAddAccount_OnClick(object sender, RoutedEventArgs e)
        {
            AccountWindow accountWindow = new AccountWindow(_context);
            accountWindow.Show();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataCore.Persistance;

namespace GUI
{
    /// <summary>
    /// Interaction logic for AccountsControl.xaml
    /// </summary>
    public partial class AccountsControl : UserControl
    {
        private TsContext Context { get; set; }

        public AccountsControl(TsContext tsContext)
        {
            InitializeComponent();
            Context = tsContext;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Context.Flats.Load();
            Context.Accounts.Load();
            Context.ProvidedServices.Load();
            Context.Services.Load();
            Context.Tariffs.Load();

            // Do not load your data at design time.
             if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
             {
             	//Load your data here and assign the result to the CollectionViewSource.
                 System.Windows.Data.CollectionViewSource myCollectionViewSource = (System.Windows.Data.CollectionViewSource)this.Resources["flatViewSource"];
                 myCollectionViewSource.Source = Context.Flats.ToList();
                 System.Windows.Data.CollectionViewSource accCollectionViewSource = (System.Windows.Data.CollectionViewSource)this.Resources["flatViewSource"];
                 myCollectionViewSource.Source = Context.Flats.ToList();
             }

        }

    }
}

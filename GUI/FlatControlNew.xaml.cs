﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataCore.Model;
using DataCore.Persistance;

namespace GUI
{
    /// <summary>
    /// Interaction logic for FlatControlNew.xaml
    /// </summary>
    public partial class FlatControlNew : UserControl
    {
        private TsContext Context { get; set; }
        public FlatControlNew(TsContext tsContext)
        {
            InitializeComponent();
            Context = tsContext;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Context.Flats.Load();
            Context.Owners.Load();
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var myCollectionViewSource =
                    (CollectionViewSource)this.Resources["flatViewSource"];
                //myCollectionViewSource.Source = Context.Flats.OrderBy(x=>x.Number).ToList();
                myCollectionViewSource.Source = Context.Flats.ToList();
                myCollectionViewSource.SortDescriptions.Add(new SortDescription("Number", ListSortDirection.Ascending));
            }
        }
        public void Delete()
        {
            Context.Accounts.Load();
            Context.ProvidedServices.Load();
            var selectedFlat = (Flat)flatListView.SelectedItem;
            if (selectedFlat != null)
            {
                while (Context.Flats.Find(selectedFlat.Id).Accounts.Count != 0)
                {
                    while (Context.Flats.Find(selectedFlat.Id).Accounts[0].ProvidedServices.Count != 0)
                    {
                        Context.Flats.Find(selectedFlat.Id).Accounts[0].ProvidedServices.Remove(
                            Context.Flats.Find(selectedFlat.Id).Accounts[0].ProvidedServices[0]);
                    }
                    Context.Flats.Find(selectedFlat.Id).Accounts.Remove(Context.Flats.Find(selectedFlat.Id).Accounts[0]);
                }
                Context.Flats.Remove(selectedFlat);
                InitializeComponent();
                Context.SaveChanges();
            }

        }
    }
}

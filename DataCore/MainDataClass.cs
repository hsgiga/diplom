﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.Entity;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataCore.Model;
using DataCore.Persistance;

namespace DataCore
{
    static public class MainDataClass
    {
        private static TsContext _tsContext;
        //Flat flat = new Flat();
        //Service service = new Service();
        public static  TsContext TsContext
        {
            get { return _tsContext; }
            private set
            {
                
            }
        }

        static MainDataClass ()
        {
            _tsContext = new TsContext();
            //
            //var init = new Initialization(new TsContext());
            //DateMonthYear dateMonth = DateMonthYear.Now;
        }

        public static void InitContext()
        {
            Database.SetInitializer<TsContext>(new Initialization(new TsContext()));
        }

        public static void Delete(Flat flat)
        {
            TsContext.Flats.Remove(flat);
            TsContext.SaveChanges();
        }

    }
}

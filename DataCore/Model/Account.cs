﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using DataCore.Model;

namespace DataCore.Model
{
    /// <summary>
    /// Класс описывающий счет
    /// </summary>
    public class Account : Entity
    {
        private ObservableCollection<ProvidedService> _providedServices;
        private int _totalmonthcost;
        private int _totaltopay;

        /// <summary>
        /// Месяц
        /// </summary>
        public byte Month { get; set; }
        /// <summary>
        /// Год
        /// </summary>
        public int Year { get; set; }
        /// <summary>
        /// Дата формирования счета
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Предоставляемые услуги
        /// </summary>
        public ObservableCollection<ProvidedService> ProvidedServices
        {
            get { return _providedServices ?? (_providedServices = new ObservableCollection<ProvidedService>()); }
            set { _providedServices = value; }
        }
        /// <summary>
        /// Итого за месяц
        /// </summary>
        public int TotalMonthCost
        {
            get { return CalculateTotalMonthCost(); }
            set { _totalmonthcost = value; }
        }
        /// <summary>
        /// Задолженность
        /// </summary>
        public int Debt { get; set; }
        /// <summary>
        /// Пеня
        /// </summary>
        public int Fine { get; set; }
        /// <summary>
        /// Итого к оплате
        /// </summary>
        public int TotalToPay
        {
            get {return _totaltopay = _totalmonthcost + Debt + Fine;}
            set { _totaltopay = value; }
        }
        /// <summary>
        /// Статус оплаты
        /// </summary>
        public bool IsPayment { get; set; }
        private int CalculateTotalMonthCost()
        {
            _totalmonthcost = 0;
            if (_providedServices != null)
            {
                foreach (var providedService in ProvidedServices)
                {
                    _totalmonthcost += Convert.ToInt32(providedService.Summ - providedService.Exemption +providedService.Recalculation);
                }
            }
            return _totalmonthcost; 
        }

        public Account(
            byte month, 
            int year, 
            DateTime date,
            int debt=0,
            int fine=0,
            bool ispayment=false)
        {
            this.Month = month;
            this.Year = year;
            this.Date = date;
            this.Debt = debt;
            this.Fine = fine;
            this.IsPayment = ispayment;
        }

        public Account()
        {
        }

        public void Add(ProvidedService providedService)
        {
            ProvidedServices.Add(providedService);
        }

        public void Calculation(Flat flat)
        {
            try
            {
                foreach (var providedService in ProvidedServices)
                {
                    var tariff = providedService.Service.GetLastTariff().Cost;

                    switch (providedService.Service.Name)
                    {
                        case "TRASH":
                            providedService.Count = CalcTrash(flat);
                            break;
                        case "LIFT":
                            providedService.Count = CalcLift(flat);
                            break;
                        case "TO":
                            providedService.Count = CalcTO(flat);
                            break;
                        case "TOQ":
                            providedService.Count = CalcToq(flat);
                            break;
                        case "OVERHAUL":
                            providedService.Count = CalcOverhaul(flat);
                            break;
                        case "DRYER":
                            providedService.Count = CalcDryer(flat);
                            break;
                        case "INTERCOM":
                            providedService.Count = CalcIntercom();
                            break;
                        case "ATTENDANT":
                            providedService.Count = CalcAttendant();
                            break;
                        case "NEEDS":
                            providedService.Count = CalcNeeds(flat);
                            break;

                    }

                    providedService.Summ = tariff * providedService.Count;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(messageBoxText: exception.Message);
                throw;
            }
        }
        /// <summary>
        /// Расчет количества предоставляемых услуг. Вывоз мусора.
        /// </summary>
        private double CalcTrash(Flat flat)
        {
            return flat.PeopleLiving > flat.PeopleRegister
                       ? flat.PeopleLiving
                       : flat.PeopleRegister;
        }
        /// <summary>
        /// Расчет количества предоставляемых услуг. Пользование лифтом.
        /// </summary>
        private double CalcLift(Flat flat)
        {
            if (flat.Floor > 2)
            {
                return flat.PeopleLiving > flat.PeopleRegister
                             ? flat.PeopleLiving
                             : flat.PeopleRegister;
            }
            return 0;
        }
        /// <summary>
        /// Расчет количества предоставляемых услуг. Техобслуживание.
        /// </summary>
        private double CalcTO(Flat flat)
        {

            if (flat.PeopleRegister > 0)
            {
                if (flat.CountRoom > 1)
                    return flat.PeopleRegister * 20 + 10;
                return flat.Area;
            }
            return 0;
        }
        /// <summary>
        /// Расчет количества предоставляемых услуг. Техобслуживание сверхнормы.
        /// </summary>
        private double CalcToq(Flat flat)
        {
            if (flat.PeopleRegister > 0)
            {
                if (flat.CountRoom > 1)
                    return flat.Area - (flat.PeopleRegister * 20 + 10);
                return 0;
            }
            return flat.Area;
        }
        /// <summary>
        /// Расчет количества предоставляемых услуг. Капитальные ремонт.
        /// </summary>
        private double CalcOverhaul(Flat flat)
        {
            return flat.Area;
        }
        /// <summary>
        /// Расчет количества предоставляемых услуг. Полотенцесушитель.
        /// </summary>
        private double CalcDryer(Flat flat)
        {
            return flat.Area;
        }
        /// <summary>
        /// Расчет количества предоставляемых услуг. Домофон.
        /// </summary>
        private double CalcIntercom()
        {
            return 1;
        }
        /// <summary>
        /// Расчет количества предоставляемых услуг. Зарплата дежурного.
        /// </summary>
        private double CalcAttendant()
        {
            return 1;
        }
        /// <summary>
        /// Расчет количества предоставляемых услуг. Хозяйственные нужды.
        /// </summary>
        private double CalcNeeds(Flat flat)
        {
            return flat.Area;
        }
    }
}

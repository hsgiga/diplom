﻿using System;

namespace DataCore.Model
{
    public struct Debt
    {
        public DateTime DateDebt { get; set; }
        public int Summ { get; set; }
        public int Fine 
        { 
            get { return CalculateFine(); }
        }

        private int CalculateFine()
        {
            const double percent = 0.001;
            var days = DateTime.Now - DateDebt;
            if (Summ > 0)
            {
                return (int)Math.Ceiling(Summ * (days.Days * percent));
            }
            return 0;
        }
    }
}
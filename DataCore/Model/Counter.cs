﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCore.Model
{
    public class Counter : Entity
    {
        private ObservableCollection<float> _data;

        public string Name { get; set; }
        public string Description { get; set; }
        public ObservableCollection<float> Data
        {
            get { return _data ?? (_data = new ObservableCollection<float>()); }
            set { _data = value; }
        }
    }
}

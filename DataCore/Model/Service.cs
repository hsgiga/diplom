﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCore.Model
{
    public class Service : Entity
    {
        private ObservableCollection<Tariff> _tariffs;
 
        public string Name { get; set; }
        public string Unit { get; set; }
        public ObservableCollection<Tariff> Tariffs
        {
            get { return _tariffs ?? (_tariffs = new ObservableCollection<Tariff>()); }
            set { _tariffs = value; }
        }
        public string Description { get; set; }

        public Service(
            string name, 
            string unit,
            Tariff tariff=null, 
            string description = null )
        {
            if (tariff != null) 
                this.Tariffs.Add(tariff);
            this.Name = name;
            this.Unit = unit;
            if (description != null)
                this.Description = description;
        }

        public Service()
        {
        }

        public Tariff GetLastTariff()
        {
            return Tariffs[0];
        }
    }
}

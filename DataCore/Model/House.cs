﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCore.Model
{
    public class House
    {
        [Key]
        public int Code { get; set; }
        public string Number { get; set; }
        public string Address { get; set; }
        public string SettlementAccount { get; set; }
        public string Unp { get; set; }
        

        private List<LogHeating> _heating;
        private List<LogHeatingWater> _heatingWater;

        /// <summary>
        /// Журнал отопления
        /// </summary>
        public List<LogHeating> Heating
        {
            get { return _heating ?? (_heating = new List<LogHeating>()); }
            set { _heating = value; }
        } 
        /// <summary>
        /// Журнал подогрева воды
        /// </summary>
        public List<LogHeatingWater> HeatingWater
        {
            get { return _heatingWater ?? (_heatingWater = new List<LogHeatingWater>()); }
            set { _heatingWater = value; }
        }

        public House()
        {
        }    

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCore.Model
{
    public class Payment : Entity
    {
        public DateTime Date { get; set; }
        public int Summ { get; set; }

        public Payment()
        {
            
        }
        public Payment(DateTime date, int summ)
        {
            this.Date = date;
            this.Summ = summ;
        }
    }
}

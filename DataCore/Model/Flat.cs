﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCore.Model
{
    public class Flat :Entity
    {
        private ObservableCollection<Tentant> _tentants;
        private ObservableCollection<Account> _accounts;
        private ObservableCollection<Payment> _payments;
        private ObservableCollection<Counter> _counters; 

        public int Number { get; set; }
        public Owner Owner { get; set; }
        public double Area { get; set; }
        public int Floor { get; set; }
        public int CountRoom { get; set; }
        public int PeopleLiving { get; set; }
        public int PeopleRegister { get; set; }
        public int Debt { get; set; }

        public ObservableCollection<Account> Accounts
        {
            get { return _accounts ?? (_accounts = new ObservableCollection<Account>()); }
            set { _accounts = value; }
        }
        public ObservableCollection<Tentant> Tentants
        {
            get { return _tentants ?? (_tentants = new ObservableCollection<Tentant>()); }
            set { _tentants = value; }
        }
        public ObservableCollection<Payment> Payments
        {
            get { return _payments ?? (_payments = new ObservableCollection<Payment>()); }
            set { _payments = value; }
        }
        public ObservableCollection<Counter> Counters
        {
            get { return _counters ?? (_counters = new ObservableCollection<Counter>()); }
            set { _counters = value; }
        }

        public Flat(
            int number,
            double area, 
            int floor=1, 
            int countRoom=1, 
            int peopleLiving=0, 
            int peopleRegister=0,
            Owner owner = null,
            Account account = null,
            Tentant tentant = null,
            Payment payment = null,
            Counter counter = null,
            int debt=0)
        {
            this.Number = number;
            this.Area = area;
            this.Floor = floor;
            this.CountRoom = countRoom;
            this.PeopleLiving = peopleLiving;
            this.PeopleRegister = peopleRegister;
            this.Owner = owner;
            this.Debt = debt;
            if (account != null)
            {
                this.Accounts.Add(account);
            }
            if (tentant != null) 
                this.Tentants.Add(tentant);
            if (payment != null) 
                this.Payments.Add(payment);
            if (counter != null)
                this.Counters.Add(counter);
        }

        public Flat()
        {
        }

        public void Add(Tentant tentant)
        {
            if (tentant != null) 
                this.Tentants.Add(tentant);
        }
        public void Add(Account account)
        {
            if (account == null) return;
            account.Calculation(this);
            this.Accounts.Add(account);
        }
        public void Add(Payment payment)
        {
            if (payment != null) 
                this.Payments.Add(payment);
        }
        public void Add(Counter counter)
        {
            if (counter != null) 
                this.Counters.Add(counter);
        }
    }
}

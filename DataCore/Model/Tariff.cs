﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCore.Model
{
    public class Tariff : Entity, IComparable<Tariff>
    {
        public double Cost { get; set; }
        public DateTime Date { get; set; }
        
        public int CompareTo (Tariff other)                                                          
        {
            int compare = this.Date.CompareTo(other.Date);
            return compare;
        }

        public Tariff (double cost, DateTime date)
        {
            this.Cost = cost;
            this.Date = date;
        }

        public Tariff()
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCore.Model
{
    /// <summary>
    /// Журналы показаний
    /// </summary>
    public class Log : Entity
    {
        private List<Heating> _heating;
        private List<Heating> _heatingWater;

        /// <summary>
        /// Журнал отопления
        /// </summary>
        public List<Heating> Heating
        {
            get { return _heating ?? (_heating = new List<Heating>()); }
            set { _heating = value; }
        } 
        /// <summary>
        /// Журнал подогрева воды
        /// </summary>
        public List<Heating> HeatingWater
        {
            get { return _heatingWater ?? (_heatingWater = new List<Heating>()); }
            set { _heatingWater = value; }
        }

        public Log()
        {
        }    
    }
}

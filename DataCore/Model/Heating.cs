﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCore.Model
{
    public class Pipeline
    {
        public float Reading { get; set; }
        public float Volume { get; set; }
        public float MomentaryConsumption { get; set; }
        public float Themperature { get; set; }
        public float WorkTime { get; set; }
    }

    public class Heating : Entity
    {
        public DateTime DateTime { get; set; }
        public Pipeline InPipeline { get; set; }
        public Pipeline OutPipeline { get; set; }
        public float WorkTimeError { get; set; }
    }
}

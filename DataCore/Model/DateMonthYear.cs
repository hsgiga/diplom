﻿using System;

namespace DataCore.Model
{
    public struct DateMonthYear
    {
        public int Year;
        public int Month;
        public static DateMonthYear Now { 
            get { return new DateMonthYear(DateTime.Now); }
        }

        public DateMonthYear(DateTime dateTime)
        {
            this.Year = dateTime.Year;
            this.Month = dateTime.Month;
        }
        public DateMonthYear(int year, int month)
        {
            if (year> 1990 && year<3000 )
            {
                this.Year = year;
            }
            else
            {
                new Exception("Year out of range");
                Year = DateTime.Now.Year;
            }

            if (month <= 12 && month > 0)
            {
                this.Month = month;
            }
            else
            {
                new Exception("Month out of range");
                Month = DateTime.Now.Month;
            }
        }

        

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCore.Model
{
    public class ProvidedService : Entity
    {
        public Service Service { get; set; }
        public double Count { get; set; }
        public double Summ { get; set; }
        public double Exemption { get; set; }
        public double Recalculation { get; set; }

        public ProvidedService()
        {
        }

        public ProvidedService (Service service)
        {
            this.Service = service;
        }
    }
}

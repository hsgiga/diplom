﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataCore.Model;

namespace DataCore.Persistance
{
    public class DataBaseTsInitializer : DropCreateDatabaseIfModelChanges<TsContext>
    {
        protected override void Seed(TsContext context)
        {
            context.SaveChanges();
        }
    }
}

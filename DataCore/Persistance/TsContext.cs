﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataCore.Model;
using System.Data.Entity;

namespace DataCore.Persistance
{
    public class TsContext : DbContext
    {
        /// <summary>
        /// Зададим имя БД
        /// </summary>
        public TsContext()
            : base("TSDB")
        {}

        public DbSet<Service> Services { get; set; }
        public DbSet<Flat> Flats { get; set; }
        public DbSet<Tariff> Tariffs { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<ProvidedService> ProvidedServices { get; set; }
        public DbSet<Owner> Owners { get; set; }
        public DbSet<Payment> Payments { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        } 

    }
}

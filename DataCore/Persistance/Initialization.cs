﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataCore.Model;

namespace DataCore.Persistance
{
    public class Initialization : DropCreateDatabaseIfModelChanges<TsContext>
    {
        public Initialization (TsContext context)
        {
            //Инициализация списка квартир
            var flats = new ObservableCollection<Flat>
                {
                    new Flat(1, 62.8, 1, 2, 1, 1),
                    new Flat(2, 48.4, 1, 1, 1, 1),
                    new Flat(3, 48.2, 1, 1),
                    new Flat(4, 72.3, 1, 2)
                };
            //Инициализация списка сервисов
            var services = new ObservableCollection<Service>
                {
                    new Service("TRASH", "ед.", new Tariff(550,DateTime.Now)),
                    new Service("LIFT","ед.",new Tariff(1421,DateTime.Now)),
                    new Service("TO","м2",new Tariff(296,DateTime.Now)),
                    new Service("TOQ","м2",new Tariff(622.5,DateTime.Now))
                };
            var accounts = new ObservableCollection<Account>
                {
                    new Account(5,2013,DateTime.Now)
                };
            foreach (var service in services)
            {
                accounts[0].Add(new ProvidedService(service));
            }

            foreach (var flat in flats)
            {
                foreach (var account in accounts)
                {
                    flat.Add(account);
                }
            }

            foreach (var flat in flats)
            {
                context.Flats.Add(flat);
            }

            foreach (var account in accounts)
            {
                context.Accounts.Add(account);
            }
            foreach (var service in services)
            {
                context.Services.Add(service);
            }
            context.SaveChanges();
        }
    }
}
